package cz.cvut.fel.www.tourify

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import cz.cvut.fel.www.tourify.api.GlobalProperties
import cz.cvut.fel.www.tourify.api.tourify.Places
import kotlinx.android.synthetic.main.fragment_trip_planner_2.*

class TripPlannerSelectPlaces : Fragment() {

    private val selected = ArrayList<Places>()
    private val properties = GlobalProperties.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_trip_planner_2, container, false)
    }

    override fun onResume() {
        super.onResume()
        vNextLabelDates.setOnClickListener({
            if (handleNextClick()) {
                fragmentManager.beginTransaction()
                        .replace(R.id.main_screen, TripPlannerNameOfTrip())
                        .addToBackStack("TripPlannerSelectPlaces")
                        .commit()
            } else {
                Toast.makeText(vNearYouList.context, "Chose at least one place", Toast.LENGTH_SHORT).show()
            }
        })

        renderListNearMe()
    }

    private fun renderListNearMe() {
        vNearYouList.adapter = TripPlannerPlacesAdapter(vNearYouList.context, R.layout.plan_trip_select_places, properties.places)
    }

    private fun handleNextClick(): Boolean {

        val places = ArrayList<Places>()
        for (place in properties.places) {
            Toast.makeText(view.context, place.name + " Is " + place.isSelected, Toast.LENGTH_SHORT).show()

            if (place.isSelected) {
                val k = place.copy()
                k.isSelected = false
                places.add(k)
            }
        }
        properties.new_trip.places = places
        return places.size > 0

    }

    private inner class TripPlannerPlacesAdapter(context: Context, textViewResourceId: Int, objects: List<Places>) : ArrayAdapter<Places>(context, textViewResourceId, objects) {

        var mIdMap = HashMap<Places, Long>()

        override fun getItemId(position: Int): Long {
            val item = getItem(position)
            return item.id.toLong()

        }


        override fun hasStableIds(): Boolean {
            return true
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var view = convertView
            val place = getItem(position)

            if (convertView == null) {
                view = LayoutInflater.from(context).inflate(R.layout.plan_trip_select_places, parent, false)
            }
            val textName = view!!.findViewById<TextView>(R.id.vPlaceTitle)
            val descName = view.findViewById<TextView>(R.id.place_desc)
            val vCheckBoxSelect = view.findViewById<CheckBox>(R.id.vCheckBoxSelect)

            textName.text = place.name
            descName.text = place.description
            vCheckBoxSelect.isSelected = place.isSelected
            vCheckBoxSelect.setOnClickListener({
                Toast.makeText(vCheckBoxSelect.context, place.name + it.findViewById<CheckBox>(R.id.vCheckBoxSelect).isChecked, Toast.LENGTH_SHORT).show()
                place.isSelected = it.findViewById<CheckBox>(R.id.vCheckBoxSelect).isChecked

            })

            return view

        }

        init {
            for ((i, item) in objects.withIndex()) {
                mIdMap[item] = i + 0L
            }
        }
    }
}