package cz.cvut.fel.www.tourify.api.tourify;

import cz.cvut.fel.www.tourify.api.tourify.Exceptions.NotRegisteredException;

public class TourifyAPI {

    private String api_token;
    private boolean is_token_valid = false;

    public TourifyAPI(String token) {
        if (token == null) {
            throw new NotRegisteredException("token is null");
        }
        api_token = token;
//        TODO: check if tokoen is valid
    }

    public TourifyAPI() {
        api_token = null;
        is_token_valid = false;

    }

    public boolean isTokenValid() {
        return is_token_valid;
    }


}

