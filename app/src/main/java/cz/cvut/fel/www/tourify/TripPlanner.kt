package cz.cvut.fel.www.tourify

import android.app.DatePickerDialog
import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import cz.cvut.fel.www.tourify.api.GlobalProperties
import cz.cvut.fel.www.tourify.api.tourify.Trip
import kotlinx.android.synthetic.main.fragment_trip_planner_1.*
import java.text.SimpleDateFormat
import java.util.*


class TripPlanner : Fragment() {


    private val properties = GlobalProperties.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trip_planner_1, container, false)

    }

    override fun onStart() {
        super.onStart()
        initializeSetDate()
    }


    private fun addCalendarModal(text: EditText) {
        val startCalendar = Calendar.getInstance()
        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->

            startCalendar.set(Calendar.YEAR, year)
            startCalendar.set(Calendar.MONTH, monthOfYear)
            startCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel(text, startCalendar)
        }

        text.setOnClickListener {
            DatePickerDialog(activity, date, startCalendar
                    .get(Calendar.YEAR), startCalendar.get(Calendar.MONTH),
                    startCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }
    }


    companion object {
        @JvmStatic
        fun newInstance() = TripPlanner().apply { }


    }

    private fun updateLabel(text: EditText, startCalendar: Calendar) {
        val myFormat = "dd.MM.yy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.GERMAN)
        text.setText(sdf.format(startCalendar.time))
    }

    private fun initializeSetDate() {
        addCalendarModal(vStartDate)
        addCalendarModal(vEndDate)
        vNextLabelDates.setOnClickListener({
            if (this.handleNextClick()) {
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.main_screen, TripPlannerSelectPlaces())
                        .addToBackStack("placesSelected")
                        .commit()
            }
        })
    }

    private fun handleNextClick(): Boolean {
        return if (vStartDate.text.isEmpty() || vEndDate.text.isEmpty()) {
            Toast.makeText(vStartDate.context, "Start or End date is not selected", Toast.LENGTH_SHORT).show()
            false
        } else {
            properties.new_trip = Trip(stringToDate(vStartDate.text.toString()), stringToDate(vEndDate.text.toString()))
            true
        }
    }

    private fun stringToDate(str: String): Date {

        val fmt = SimpleDateFormat("dd.MM.yy")
        return fmt.parse(str)
    }
}

