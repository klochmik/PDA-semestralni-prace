package cz.cvut.fel.www.tourify.api.tourify.Exceptions;

public class NotRegisteredException extends RuntimeException {
    public NotRegisteredException() {
        super();
    }

    public NotRegisteredException(String message) {
        super(message);
    }

    public NotRegisteredException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotRegisteredException(Throwable cause) {
        super(cause);
    }
}
