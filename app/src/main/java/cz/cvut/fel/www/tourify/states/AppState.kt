package cz.cvut.fel.www.tourify.states

import cz.cvut.fel.www.tourify.states.login.LoginState
import java.io.Serializable

data class AppState(
        val loginState: LoginState

) : Serializable