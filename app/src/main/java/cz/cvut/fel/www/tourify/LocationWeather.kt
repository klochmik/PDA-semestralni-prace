package cz.cvut.fel.www.tourify


import android.os.Bundle
import android.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


/**
 * A simple [Fragment] subclass.
 */
class LocationWeather : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_location_weather, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance() = LocationWeather().apply { }
    }

}// Required empty public constructor
