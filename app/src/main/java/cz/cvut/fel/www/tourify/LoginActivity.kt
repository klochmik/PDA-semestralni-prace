package cz.cvut.fel.www.tourify

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import cz.cvut.fel.www.tourify.api.GlobalProperties
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {


    private val globalProperties = GlobalProperties.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        vGoogleLogin.setOnClickListener {
            globalProperties.loggedin = true
            val pref = this.getSharedPreferences(getString(R.string.is_logged_id), android.content.Context.MODE_PRIVATE)
            with(pref.edit()) {
                putBoolean(Main.IS_LOGGED_ID, true)
                commit()
            }

            this.finish()
        }
    }


    private fun isEmailValid(email: String): Boolean {
        //TODO: Replace this with your own logic
        return email.contains("@")
    }

    private fun isPasswordValid(password: String): Boolean {
        //TODO: Replace this with your own logic
        return password.length > 4
    }

    companion object {

        /**
         * Id to identity READ_CONTACTS permission request.
         */
        private val REQUEST_READ_CONTACTS = 0

        /**
         * A dummy authentication store containing known user names and passwords.
         * TODO: remove after connecting to a real authentication system.
         */
        private val DUMMY_CREDENTIALS = arrayOf("foo@example.com:hello", "bar@example.com:world")

        @JvmStatic
        fun newInstance() = LoginActivity().apply { }
    }

    override fun onBackPressed() {

    }
}
