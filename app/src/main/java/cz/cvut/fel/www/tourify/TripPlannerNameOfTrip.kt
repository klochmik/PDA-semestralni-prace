package cz.cvut.fel.www.tourify

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.cvut.fel.www.tourify.api.GlobalProperties
import kotlinx.android.synthetic.main.fragment_trip_planner_4.*

class TripPlannerNameOfTrip : Fragment() {
    private val properties = GlobalProperties.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_trip_planner_4, container, false)
    }

    override fun onResume() {
        super.onResume()
        vFinishTrip.setOnClickListener({
            properties.new_trip.name = vNameTripInput.text.toString()
            properties.trips.add(properties.new_trip)

            for (i in 0 until fragmentManager.backStackEntryCount) {
                fragmentManager.popBackStack()
            }

            fragmentManager.beginTransaction().replace(R.id.main_screen, HomePage()).commit()
        })
    }
}