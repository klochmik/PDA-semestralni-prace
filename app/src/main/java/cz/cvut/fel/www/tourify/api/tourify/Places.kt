package cz.cvut.fel.www.tourify.api.tourify

data class Places(
        val id: Int,
        val name: String,
        val gps: GPS,
        val description: String,
        var isSelected: Boolean = false
)