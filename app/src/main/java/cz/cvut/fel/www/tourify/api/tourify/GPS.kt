package cz.cvut.fel.www.tourify.api.tourify

data class GPS(
        val longitude: Double,
        val latitude: Double
)

