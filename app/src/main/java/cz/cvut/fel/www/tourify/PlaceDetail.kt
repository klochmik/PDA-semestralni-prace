package cz.cvut.fel.www.tourify

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import cz.cvut.fel.www.tourify.api.tourify.Places


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PlaceDetail.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [PlaceDetail.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlaceDetail : Fragment() {

    var place: Places? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_place_detail, container, false)

        val name = view.findViewById<TextView>(R.id.vPlaceTitle)
        name.text = place!!.name

        val description = view.findViewById<TextView>(R.id.place_desc)
        description.text = place!!.description

        return view
    }


    companion object {


        fun newInstance(place: Places): PlaceDetail {
            val fragment = PlaceDetail()
            fragment.place = place
            return fragment
        }
    }
}// Required empty public constructor
