package cz.cvut.fel.www.tourify.api.tourify

import java.util.*

data class Trip(
        var dateStart: Date,
        var dateEnd: Date,
        var places: List<Places>? = ArrayList(),
        var name: String? = ""
)