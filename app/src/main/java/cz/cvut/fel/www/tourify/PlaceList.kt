package cz.cvut.fel.www.tourify

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import cz.cvut.fel.www.tourify.api.GlobalProperties
import cz.cvut.fel.www.tourify.api.tourify.Places
import cz.cvut.fel.www.tourify.api.tourify.Trip


class PlaceList : Fragment() {


    private val properties = GlobalProperties.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_place_list, container, false)
        val list = view.findViewById<ListView>(R.id.vListOfPlaces1)
        list.adapter = MyPlaceAdapter(view.context, R.layout.place_item, properties.places)
        return view
    }

    private inner class MyPlaceAdapter(context: Context, textViewResourceId: Int, objects: List<Places>) : ArrayAdapter<Places>(context, textViewResourceId, objects) {

        var mIdMap = HashMap<Places, Long>()

        override fun getItemId(position: Int): Long {
            val item = getItem(position)

            return item.id.toLong()

        }


        override fun hasStableIds(): Boolean {
            return true
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var view = convertView
            val place = getItem(position)

            if (convertView == null) {
                view = LayoutInflater.from(context).inflate(R.layout.place_item, parent, false)
            }
            val textName = view!!.findViewById<TextView>(R.id.vPlaceTitle)
            textName.text = place.name
            val photo = view.findViewById<ConstraintLayout>(R.id.place_photo)
            photo.setOnClickListener({
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.main_screen, PlaceDetail.newInstance(place))
                        .addToBackStack("viewPlace")
                        .commit()
            })


            return view

        }

        init {
            for ((i, item) in objects.withIndex()) {
                mIdMap[item] = i + 0L
            }
        }

        private fun onSelectItem(trip: Trip) {
//            TODO: open map with places


        }
    }


    companion object {
        fun newInstance(): PlaceList {
            val fragment = PlaceList()
            return fragment
        }
    }
}// Required empty public constructor
