package cz.cvut.fel.www.tourify.states.login

data class LoginState(
        var isLoggedIn: Boolean
)