package cz.cvut.fel.www.tourify

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.TextView
import cz.cvut.fel.www.tourify.api.GlobalProperties
import cz.cvut.fel.www.tourify.api.tourify.Places
import kotlinx.android.synthetic.main.fragment_trip_planner_3.*

class TripPlannerSelectPlacesDate : Fragment() {

    private val properties = GlobalProperties.getInstance()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_trip_planner_3, container, false)
    }

    override fun onResume() {
        super.onResume()
        vNextLabelDates.setOnClickListener({
            if (this.hanldeClick()) {
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.main_screen, TripPlannerNameOfTrip())
                        .addToBackStack("SelectNameOfTrip")
                        .commit()
            }
        })
        vListSelected.adapter = TripPlannerPlacesTimeAdapter(vListSelected.context, R.layout.trip_placec_date_selected_item, properties.new_trip.places!!)

    }

    private fun hanldeClick(): Boolean {
        return true
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private inner class TripPlannerPlacesTimeAdapter(context: Context, textViewResourceId: Int, objects: List<Places>) : ArrayAdapter<Places>(context, textViewResourceId, objects) {

        var mIdMap = HashMap<Places, Long>()

        override fun getItemId(position: Int): Long {
            val item = getItem(position)
            return item.id.toLong()

        }


        override fun hasStableIds(): Boolean {
            return true
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var view = convertView
            val place = getItem(position)

            if (convertView == null) {
                view = LayoutInflater.from(context).inflate(R.layout.trip_placec_date_selected_item, parent, false)
            }
            val textName = view!!.findViewById<TextView>(R.id.vPlaceTitle)
            val from = view.findViewById<EditText>(R.id.vStartDate)
            val to = view.findViewById<EditText>(R.id.end_date_input)

            textName.text = place.name
            return view

        }

        init {
            for ((i, item) in objects.withIndex()) {
                mIdMap[item] = i + 0L
            }
        }
    }
}