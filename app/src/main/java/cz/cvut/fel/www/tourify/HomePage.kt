package cz.cvut.fel.www.tourify

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import cz.cvut.fel.www.tourify.api.GlobalProperties
import kotlinx.android.synthetic.main.fragment_home_page.*


/**
 * [Fragment]
 */
class HomePage : Fragment() {

    private val properties: GlobalProperties = GlobalProperties.getInstance()

    private val make_toasts = true
    private var location_of_user: String? = null

    private var notificationManager: NotificationManager? = null


    init {
        // Required empty public constructor
    }

    private fun initializeProperties() {
        this.location_of_user = properties.location
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_home_page, container, false)

        this.initializeProperties()
        activity.title = this.location_of_user

        // Notification
        notificationManager =
                activity.getSystemService(
                        Context.NOTIFICATION_SERVICE) as NotificationManager

        createNotificationChannel(
                "cz.cvut.fel.www.tourify",
                "Tourify App",
                "Awesome app")

        sendNotification()

        return view
    }


    private fun toast(message: String) {
        if (make_toasts) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun btnChangeLocationHandler(v: View) {
        val transaction = fragmentManager.beginTransaction()
        val searchFragment = LocationSearchFragment.newInstance()
        transaction.replace(R.id.main_screen, searchFragment)
        transaction.addToBackStack("search_city")
        transaction.commit()
        toast("Prechod na vyhledavni mest")
    }

    private fun btnPlacesHandler(v: View) {
        val transaction = fragmentManager.beginTransaction()
        val searchFragment = PlaceList.newInstance()
        transaction.replace(R.id.main_screen, searchFragment)
        transaction.addToBackStack("places")
        transaction.commit()
        toast("Prechod na vyhledavni mist")
    }

    private fun btnPlanTripHandler(v: View) {
        val transaction = fragmentManager.beginTransaction()
        val searchFragment = TripPlanner.newInstance()
        transaction.replace(R.id.main_screen, searchFragment)
        transaction.addToBackStack("plan_trip")
        transaction.commit()
        toast("Prechod na planocani cesty")
    }

    private fun btnMapHandler(v: View) {
        val intent = Intent(activity, PlaceMap::class.java)
        startActivity(intent)

        toast("Prechod na mapu mist")
    }

    private fun btnAboutHandler(v: View) {
        val transaction = fragmentManager.beginTransaction()
        val searchFragment = LocationAbout.newInstance()
        transaction.replace(R.id.main_screen, searchFragment)
        transaction.addToBackStack("location_about")
        transaction.commit()
        toast("Prechod na informaci o meste")
    }

    companion object {

        @JvmStatic
        fun hideKeyboard(activity: Activity) {
            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            var view = activity.currentFocus
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        private val ARG_PARAM1 = "location_of_user"

        fun newInstance(city: String): HomePage {
            val fragment = HomePage()
            val args = Bundle()
            args.putString(ARG_PARAM1, city)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onResume() {
        super.onResume()
        vIAmHere.text = "I am here: " + properties.location
        vHomePageChangeLocation.setOnClickListener { v -> btnChangeLocationHandler(v) }
        vBtnAbout.setOnClickListener { v -> btnAboutHandler(v) }
        vBtnPlaces.setOnClickListener { v -> btnPlacesHandler(v) }
        vBtnPlanTrip.setOnClickListener { v -> btnPlanTripHandler(v) }
        vBtnMap.setOnClickListener { v -> btnMapHandler(v) }
        HomePage.hideKeyboard(activity)

    }

    private fun createNotificationChannel(id: String, name: String,
                                          description: String) {

        val importance = NotificationManager.IMPORTANCE_LOW
        val channel = NotificationChannel(id, name, importance)

        channel.description = description
        channel.enableLights(true)
        channel.lightColor = Color.RED
        channel.enableVibration(true)
        channel.vibrationPattern =
                longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
        notificationManager?.createNotificationChannel(channel)
    }

    private fun sendNotification() {

        val notificationID = 101

        val channelID = "cz.cvut.fel.www.tourify"

        val notification = Notification.Builder(activity,
                channelID)
                .setContentTitle("Welcome to Tourify App")
                .setContentText("Your location was set to $location_of_user")
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setChannelId(channelID)
                .build()

        notificationManager?.notify(notificationID, notification)
    }


}
