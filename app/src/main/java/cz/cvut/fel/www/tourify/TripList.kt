package cz.cvut.fel.www.tourify

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import cz.cvut.fel.www.tourify.api.GlobalProperties
import cz.cvut.fel.www.tourify.api.tourify.Trip
import cz.cvut.fel.www.tourify.dummy.DummyContent.DummyItem
import java.text.SimpleDateFormat

/**
 * A fragment representing a list of Items.
 *
 *
 * Activities containing this fragment MUST implement the [OnListFragmentInteractionListener]
 * interface.
 */
/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class TripList : Fragment() {

    private val properties = GlobalProperties.getInstance()

    private var mListener: OnListFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_trip_list, container, false)


        val list = view.findViewById<ListView>(R.id.list)
        list.adapter = MyTripsAdapter(view.context, R.layout.fragment_trip_item, properties.trips)

//        // Set the adapter
//        if (view is RecyclerView) {
//            val context = view.getContext()
//            if (mColumnCount <= 1) {
//                view.layoutManager = LinearLayoutManager(context)
//            } else {
//                view.layoutManager = GridLayoutManager(context, mColumnCount)
//            }
//        }
        activity.title = "My trips"
        return view
    }


    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: DummyItem)
    }

    private inner class MyTripsAdapter(context: Context, textViewResourceId: Int, objects: List<Trip>) : ArrayAdapter<Trip>(context, textViewResourceId, objects) {

        var mIdMap = HashMap<Trip, Long>()

        override fun getItemId(position: Int): Long {
            val item = getItem(position)
            return mIdMap[item]!!

        }


        override fun hasStableIds(): Boolean {
            return true
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var view = convertView
            val trip = getItem(position)

            if (convertView == null) {
                view = LayoutInflater.from(context).inflate(R.layout.fragment_trip_item, parent, false)
            }
            val textName = view!!.findViewById<TextView>(R.id.vTripListName)
            val datesText = view.findViewById<TextView>(R.id.trip_dates)
            val deleteBtn = view.findViewById<Button>(R.id.delete_btn)

            val sdf = SimpleDateFormat("dd.MM.yy")
            val s = sdf.format(trip.dateStart)
            val e = sdf.format(trip.dateEnd)
            textName.text = trip.name
            datesText.text = "$s -- $e"

            deleteBtn.setOnClickListener({
                val toBeDeleted = properties.trips.find { trip1 -> trip.name == trip1.name }
                properties.trips.remove(toBeDeleted)
                this.notifyDataSetChanged()
            })

            textName.setOnClickListener({
                this.onSelectItem(trip)
            })
            datesText.setOnClickListener({
                this.onSelectItem(trip)
            })



            return view

        }

        init {
            for ((i, item) in objects.withIndex()) {
                mIdMap[item] = i + 0L
            }
        }

        private fun onSelectItem(trip: Trip) {
//            TODO: open map with places


        }
    }


}
