package cz.cvut.fel.www.tourify

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import cz.cvut.fel.www.tourify.api.GlobalProperties
import kotlinx.android.synthetic.main.fragment_location_search.*


class LocationSearchFragment : Fragment() {

    private val properties: GlobalProperties = GlobalProperties.getInstance()
    private var nearCities: List<String> = properties.cities
    private var searchText = ""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_location_search, container, false)
    }

    override fun onResume() {
        super.onResume()
        initSearch()
        getListNearMe()

    }

    private fun initSearch() {
        vSearchInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                searchText = p0.toString()
                getListNearMe()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun getListNearMe() {
        val iteratee: List<String> = if (searchText == "") {
            properties.cities
        } else {
            properties.cities.filter { it.contains(Regex("(?i:.*$searchText.*)")) }.toList()
        }

        val adapter = CitiesAdapter(vNearYouList.context, R.layout.adapter_list_cities, iteratee)
        vNearYouList.adapter = adapter
        vNearYouList.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val item = parent.getItemAtPosition(position) as String
            properties.location = item
//            Toast.makeText(vNearYouList.context, item, Toast.LENGTH_LONG).show()
            fragmentManager.beginTransaction().replace(R.id.main_screen, HomePage()).commit()
        }

    }

    companion object {
        @JvmStatic
        fun newInstance() = LocationSearchFragment().apply { }
    }

    private inner class CitiesAdapter(context: Context, textViewResourceId: Int, objects: List<String>) : ArrayAdapter<String>(context, textViewResourceId, objects) {

        var mIdMap = HashMap<String, Long>()

        override fun getItemId(position: Int): Long {
            val item = getItem(position)

            return mIdMap[item]!!
        }

        override fun hasStableIds(): Boolean {
            return true
        }

        init {
            for ((i, item) in objects.withIndex()) {
                mIdMap[item] = i + 0L
            }
        }
    }
}

