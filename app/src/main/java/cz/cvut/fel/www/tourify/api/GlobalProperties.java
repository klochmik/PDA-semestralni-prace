package cz.cvut.fel.www.tourify.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.cvut.fel.www.tourify.api.tourify.Mock;
import cz.cvut.fel.www.tourify.api.tourify.Places;
import cz.cvut.fel.www.tourify.api.tourify.TourifyAPI;
import cz.cvut.fel.www.tourify.api.tourify.Trip;

public class GlobalProperties {

    private static GlobalProperties instance = null;

    private final TourifyAPI tourifyAPI;

    public List<String> cities = Arrays.asList("Prague", "Pardubice", "Plzen", "Brno", "Hradec Kralove");

    private Boolean loggedin = false;

    public final List<Places> places = Mock.getPlaces();

    public String location = "Prague";

    private List<Trip> trips = new ArrayList<>();

    public Trip new_trip = null;

    private GlobalProperties() {
        tourifyAPI = new TourifyAPI();
    }

    public static GlobalProperties getInstance() {

        if (instance == null) {
            instance = new GlobalProperties();
        }
        return instance;
    }

    public Boolean getLoggedin() {
        return loggedin;
    }

    public void setLoggedin(Boolean loggedin) {
        this.loggedin = loggedin;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }
}
