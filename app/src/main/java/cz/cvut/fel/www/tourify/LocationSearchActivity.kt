package cz.cvut.fel.www.tourify

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class LocationSearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_search)
        title = "Search location"
    }
}
